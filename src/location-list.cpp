/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * This file is part of NAC (Name-Based Access Control for NDN).
 * See AUTHORS.md for complete list of NAC authors and contributors.
 *
 * NAC is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * NAC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * NAC, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Zhiyi Zhang <zhiyi@cs.ucla.edu>
 */

#include "location-list.hpp"

#include <ndn-cxx/encoding/block-helpers.hpp>
#include <ndn-cxx/util/concepts.hpp>

#include <iostream>

namespace ndn {
namespace nac {

static void
calLocationResult(const std::set<Location>& list,
                  const double& latitude, 
                  const double& longitude,
                  const double& radius,
                  Location& positiveR,
                  Location& negativeR)
{
  Location tempLocation;
  bool isPositive;

  for (const Location& element : list) {
    std::tie(isPositive, tempLocation) = element.getLocation(latitude, longitude, radius);

    if (isPositive == true) {
      positiveR = tempLocation;
      break;
    }
    else { 
        negativeR = tempLocation;
    }
  }

}

BOOST_CONCEPT_ASSERT((WireEncodable<LocationList>));
BOOST_CONCEPT_ASSERT((WireDecodable<LocationList>));

LocationList::LocationList() = default;

LocationList::LocationList(const Block& block)
{
  wireDecode(block);
}

template<encoding::Tag TAG>
size_t
LocationList::wireEncode(EncodingImpl<TAG>& encoder) const
{
  size_t totalLength = 0;
  size_t blackLength = 0;
  size_t whiteLength = 0;

  // encode the blackIntervalList as an embed TLV structure
  for (auto it = m_blackLocation.rbegin(); it != m_blackLocation.rend(); it++) {
    blackLength += encoder.prependBlock(it->wireEncode());
  }
  blackLength += encoder.prependVarNumber(blackLength);
  blackLength += encoder.prependVarNumber(tlv::BlackLocationList);

  // encode the whiteIntervalList as an embed TLV structure
  for (auto it = m_whiteLocation.rbegin(); it != m_whiteLocation.rend(); it++) {
    whiteLength += encoder.prependBlock(it->wireEncode());
  }
  whiteLength += encoder.prependVarNumber(whiteLength);
  whiteLength += encoder.prependVarNumber(tlv::WhiteLocationList);

  totalLength = whiteLength + blackLength;
  totalLength += encoder.prependVarNumber(totalLength);
  totalLength += encoder.prependVarNumber(tlv::LocationList);

  return totalLength;
}

const Block&
LocationList::wireEncode() const
{
  if (m_wire.hasWire())
    return m_wire;

  EncodingEstimator estimator;
  size_t estimatedSize = wireEncode(estimator);

  EncodingBuffer buffer(estimatedSize, 0);
  wireEncode(buffer);

  this->m_wire = buffer.block();
  return m_wire;
}

void
LocationList::wireDecode(const Block& wire)
{
  if (wire.type() != tlv::LocationList)
    BOOST_THROW_EXCEPTION(tlv::Error("Unexpected TLV type when decoding LocationList"));

  m_wire = wire;
  m_wire.parse();

  if (m_wire.elements_size() != 2)
    BOOST_THROW_EXCEPTION(tlv::Error("LocationList tlv does not have two sub-TLVs"));

  Block::element_const_iterator it = m_wire.elements_begin();

  if (it != m_wire.elements_end() && it->type() == tlv::WhiteLocationList) {
    it->parse();
    Block::element_const_iterator tempIt = it->elements_begin();
    while (tempIt != it->elements_end() && tempIt->type() == tlv::Location) {
      m_whiteLocation.insert(Location(*tempIt));
      tempIt++;
    }
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("The first element must be WhiteLocationList"));

  if (it != m_wire.elements_end() && it->type() == tlv::BlackLocationList) {
    it->parse();
    Block::element_const_iterator tempIt = it->elements_begin();
    while (tempIt != it->elements_end() && tempIt->type() == tlv::Location) {
      m_blackLocation.insert(Location(*tempIt));
      tempIt++;
    }
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("The second element must be BlackLocationList"));
}

LocationList&
LocationList::addWhiteLocation(const Location& location)
{
  m_wire.reset();
  m_whiteLocation.insert(location);
   std::cout << "addWhiteLocation :  " << location.getLatitude() << " size: "<< m_whiteLocation.size()<< std::endl;
  return *this;
}

LocationList&
LocationList::addBlackLocation(const Location& location)
{
  m_wire.reset();
  m_blackLocation.insert(location);
  return *this;
}

std::tuple<bool, Location>
LocationList::getCoveringLocation(const double& latitude, const double& longitude, const double& radius) const
{
  Location blackPositiveResult(true);
  Location whitePositiveResult(true);

  Location blackNegativeResult;
  Location whiteNegativeResult;


  Location tempLocation;

  // get the blackResult
  calLocationResult(m_blackLocation, latitude, longitude, radius, blackPositiveResult, blackNegativeResult);

  // if black positive result is not empty, the result must be false
  if (blackPositiveResult.getRadius() != 0)
    return std::make_tuple(false, blackPositiveResult);

  // get the whiteResult
  calLocationResult(m_whiteLocation, latitude, longitude, radius, whitePositiveResult, whiteNegativeResult);

  if (whitePositiveResult.getRadius()!=0) {
    // there is white interval covering the timestamp
    // return ture 
      return std::make_tuple(true, whitePositiveResult);
  }
  else {
    // there is no white interval covering the timestamp
    // return false
    return std::make_tuple(false, whiteNegativeResult);
  }
}

} // namespace nac
} // namespace ndn
