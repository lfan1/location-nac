/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * This file is part of NAC (Name-Based Access Control for NDN).
 * See AUTHORS.md for complete list of NAC authors and contributors.
 *
 * NAC is free software: you can redistribute it and/or modify it under the terms
 * of the GNU General Public License as published by the Free Software Foundation,
 * either version 3 of the License, or (at your option) any later version.
 *
 * NAC is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 * PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with
 * NAC, e.g., in COPYING.md file.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @author Zhiyi Zhang <zhiyi@cs.ucla.edu>
 */

#include "time-location.hpp"
#include "interval.hpp"
#include "location.hpp"
#include <ndn-cxx/encoding/block-helpers.hpp>

#include <math.h>  
#include <algorithm>
#include <iostream>

namespace ndn {
namespace nac {

static const TimeStamp DEFAULT_TIME = boost::posix_time::from_iso_string("14000101T000000");

BOOST_CONCEPT_ASSERT((WireEncodable<TimeLocation>));
BOOST_CONCEPT_ASSERT((WireDecodable<TimeLocation>));

TimeLocation::TimeLocation(bool isValid)
  : m_latitude(43.6121)
  , m_longitude(-116.3915)
  , m_radius(0)
  , m_startTime(DEFAULT_TIME)
  , m_endTime(DEFAULT_TIME)
  , m_isValid(isValid)
{
}

TimeLocation::TimeLocation(const Block& block)
{
  wireDecode(block);
}

TimeLocation::TimeLocation(const double& latitude, 
                           const double& longitude, 
                           const double& radius,
                           const TimeStamp& startTime, 
                           const TimeStamp& endTime)
  : m_latitude(latitude)
  , m_longitude(longitude)
  , m_radius(radius)
  , m_startTime(startTime)
  , m_endTime(endTime)
  , m_isValid(true)
{
  BOOST_ASSERT(startTime < endTime);
}

TimeLocation::TimeLocation(const double& latitude, 
                            const double& longitude, 
                            const double& radius,
                            const TimeStamp& startDate,
                            const TimeStamp& endDate,
                            size_t intervalStartHour,
                            size_t intervalEndHour,
                            size_t nRepeats,
                            RepeatUnit unit)
  : m_latitude(latitude)
  , m_longitude(longitude)
  , m_radius(radius)
  , m_startDate(startDate)
  , m_endDate(endDate)
  , m_intervalStartHour(intervalStartHour)
  , m_intervalEndHour(intervalEndHour)
  , m_nRepeats(nRepeats)
  , m_unit(unit)
  , m_isValid(true)
{
  BOOST_ASSERT(m_intervalStartHour < m_intervalEndHour);
  BOOST_ASSERT(m_startDate.date() <= m_endDate.date());
  BOOST_ASSERT(m_intervalEndHour <= 24);
  if (unit == RepeatUnit::NONE)
    BOOST_ASSERT(m_startDate.date() == m_endDate.date());
}

template<encoding::Tag TAG>
size_t
TimeLocation::wireEncode(EncodingImpl<TAG>& encoder) const
{
  size_t totalLength = 0;

  // RepeatUnit
  totalLength +=
    prependNonNegativeIntegerBlock(encoder, tlv::RepeatUnit, static_cast<size_t>(m_unit));
  // NRepeat
  totalLength += prependNonNegativeIntegerBlock(encoder, tlv::NRepeats, m_nRepeats);
  // IntervalEndHour
  totalLength += prependNonNegativeIntegerBlock(encoder, tlv::IntervalEndHour, m_intervalEndHour);
  // IntervalStartHour
  totalLength += prependNonNegativeIntegerBlock(encoder, tlv::IntervalStartHour, m_intervalStartHour);
  // EndDate
  totalLength += prependStringBlock(encoder, tlv::EndDate, to_iso_string(m_endDate));
  // StartDate
  totalLength += prependStringBlock(encoder, tlv::StartDate, to_iso_string(m_startDate));

  totalLength += prependDoubleBlock(encoder, tlv::Radius, m_radius);
  totalLength += prependDoubleBlock(encoder, tlv::CenterLongitude, m_longitude);
  totalLength += prependDoubleBlock(encoder, tlv::CenterLatitude, m_latitude);

  totalLength += encoder.prependVarNumber(totalLength);
  totalLength += encoder.prependVarNumber(tlv::TimeLocation);

  return totalLength;
}

const Block&
TimeLocation::wireEncode() const
{
  if (m_wire.hasWire())
    return m_wire;

  EncodingEstimator estimator;
  size_t estimatedSize = wireEncode(estimator);

  EncodingBuffer buffer(estimatedSize, 0);
  wireEncode(buffer);

  this->m_wire = buffer.block();
  return m_wire;
}

void
TimeLocation::wireDecode(const Block& wire)
{
  using namespace boost::posix_time;

  if (wire.type() != tlv::TimeLocation)
    BOOST_THROW_EXCEPTION(tlv::Error("Unexpected TLV type when decoding TimeLocation"));

  m_wire = wire;
  m_wire.parse();

  if (m_wire.elements_size() != 9)
    BOOST_THROW_EXCEPTION(tlv::Error("TimeLocation tlv does not have six sub-TLVs"));

  Block::element_const_iterator it = m_wire.elements_begin();

  if (it->type() == tlv::CenterLatitude) {
    m_latitude = encoding::readDouble(*it);
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("First element must be CenterLatitude"));


  if (it->type() == tlv::CenterLongitude) {
    m_longitude = encoding::readDouble(*it);
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("Second element must be CenterLongitude"));

  if (it->type() == tlv::Radius) {
    m_radius = encoding::readDouble(*it);
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("Third element must be Radius"));

  // StartDate
  if (it->type() == tlv::StartDate) {
    m_startDate = ptime(from_iso_string(readString(*it)));
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("First element must be StartDate"));

  // EndDate
  if (it->type() == tlv::EndDate) {
    m_endDate = ptime(from_iso_string(readString(*it)));
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("Second element must be EndDate"));

  // IntervalStartHour
  if (it->type() == tlv::IntervalStartHour) {
    m_intervalStartHour = readNonNegativeInteger(*it);
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("Third element must be IntervalStartHour"));

  // IntervalEndHour
  if (it->type() == tlv::IntervalEndHour) {
    m_intervalEndHour = readNonNegativeInteger(*it);
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("Fourth element must be IntervalEndHour"));

  // NRepeats
  if (it->type() == tlv::NRepeats) {
    m_nRepeats = readNonNegativeInteger(*it);
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("Fifth element must be NRepeats"));

  // RepeatUnit
  if (it->type() == tlv::RepeatUnit) {
    m_unit = static_cast<RepeatUnit>(readNonNegativeInteger(*it));
    it++;
  }
  else
    BOOST_THROW_EXCEPTION(tlv::Error("The last element must be RepeatUnit"));

}

std::tuple<bool, TimeLocation>
TimeLocation::getTimeLocation(const TimeStamp& tp, const double& latitude, const double& longitude, const double& radius) const
{
  double lat;
  double lon;
  double rad;
  TimeStamp startTime;
  TimeStamp endTime;
  bool isPositive = false;
  bool isLPositive;
  bool isTPositive;

  if (m_latitude == latitude && m_longitude == longitude && m_radius == radius) 
  {
    isLPositive = true;
    lat = m_latitude;
    lon = m_longitude;
    rad = m_radius;
  }
  else if (m_latitude == 0 && m_longitude ==0 && m_radius == 0) {
    isLPositive = true;
    lat = latitude;
    lon = longitude;
    rad = radius;

  }
  else {
    lat = m_latitude;
    lon = m_longitude;
    rad = 0;
    isLPositive = false;
  }

  if (!this->hasIntervalOnDate(tp)) {
    // there is no interval on the date of tp
    startTime = TimeStamp(tp.date(), boost::posix_time::hours(0));
    endTime = TimeStamp(tp.date(), boost::posix_time::hours(24));
    isTPositive = false;
  }
  else {
    // there is an interval on the date of tp
    startTime = TimeStamp(tp.date(), boost::posix_time::hours(m_intervalStartHour));
    endTime = TimeStamp(tp.date(), boost::posix_time::hours(m_intervalEndHour));

    // check if in the time duration
    if (tp < startTime) {
      endTime = startTime;
      startTime = TimeStamp(tp.date(), boost::posix_time::hours(0));
      isTPositive = false;
    }
    else if (tp > endTime) {
      startTime = endTime;
      endTime = TimeStamp(tp.date(), boost::posix_time::hours(24));
      isTPositive = false;
    }
    else {
      isTPositive = true;

    }
  }

  if (isTPositive && isLPositive) {
      isPositive = true;
  }

  return std::make_tuple(isPositive, TimeLocation(lat, lon, rad, startTime, endTime));
}

bool
TimeLocation::hasIntervalOnDate(const TimeStamp& tp) const
{
  namespace bg = boost::gregorian;

  // check if in the bound of the interval
  if (tp.date() < m_startDate.date() || tp.date() > m_endDate.date()) {
    return false;
  }

  if (m_unit == RepeatUnit::NONE) {
    return true;
  }

  // check if in the matching date
  bg::date dateA = tp.date();
  bg::date dateB = m_startDate.date();
  if (m_unit == RepeatUnit::DAY) {
    bg::date_duration duration = dateA - dateB;
    if (static_cast<size_t>(duration.days()) % m_nRepeats == 0)
      return true;
  }
  else if (m_unit == RepeatUnit::MONTH && dateA.day() == dateB.day()) {
    size_t yearDiff = static_cast<size_t>(dateA.year() - dateB.year());
    size_t monthDiff = 12 * yearDiff + dateA.month().as_number() - dateB.month().as_number();
    if (monthDiff % m_nRepeats == 0)
      return true;
  }
  else if (m_unit == RepeatUnit::YEAR &&
           dateA.day().as_number() == dateB.day().as_number() &&
           dateA.month().as_number() == dateB.month().as_number()) {
    size_t diff = static_cast<size_t>(dateA.year() - dateB.year());
    if (diff % m_nRepeats == 0)
      return true;
  }

  return false;
}

TimeLocation&
TimeLocation::operator&&(const TimeLocation& timeLocation)
{
  BOOST_ASSERT(isValid() && timeLocation.isValid());

  // if one is empty, result is empty
  if (isEmpty() || timeLocation.isEmpty()) {
    m_startTime = m_endTime;
    return *this;
  }
  // two intervals do not have intersection
  if (m_startTime >= timeLocation.getEndTime() || m_endTime <= timeLocation.getStartTime()) {
    m_startTime = m_endTime;
    return *this;
  }

  // get the start time
  if (m_startTime <= timeLocation.getStartTime())
    m_startTime = timeLocation.getStartTime();

  // get the end time
  if (m_endTime > timeLocation.getEndTime())
    m_endTime = timeLocation.getEndTime();

  return *this;
}

TimeLocation&
TimeLocation::operator||(const TimeLocation& timeLocation)
{
  BOOST_ASSERT(this->isValid() && timeLocation.isValid());

  if (isEmpty()) {
    // left interval is empty, return left one
    m_startTime = timeLocation.getStartTime();
    m_endTime = timeLocation.getEndTime();
    return *this;
  }
  if (timeLocation.isEmpty()) {
    // right interval is empty, return right one
    return *this;
  }
  if (m_startTime >= timeLocation.getEndTime() || m_endTime <= timeLocation.getStartTime()) {
    // two intervals do not have intersection
    BOOST_THROW_EXCEPTION(Error("cannot generate a union interval when there's no intersection"));
  }

  // get the start time
  if (m_startTime > timeLocation.getStartTime())
    m_startTime = timeLocation.getStartTime();

  // get the end time
  if (m_endTime <= timeLocation.getEndTime())
    m_endTime = timeLocation.getEndTime();

  return *this;
}

bool
TimeLocation::operator<(const TimeLocation& location) const
{
  if (m_latitude < location.getLatitude())
    return true;
  else if (m_latitude > location.getLatitude())
    return false;

  if (m_longitude < location.getLongitude())
    return true;
  else if (m_longitude > location.getLongitude())
    return false;

  if (m_radius < location.getRadius())
    return true;
  else if (m_radius > location.getRadius())
    return false;

  if (m_startDate < location.getStartDate())
    return true;
  else if (m_startDate > location.getStartDate())
    return false;

  if (m_endDate < location.getEndDate())
    return true;
  else if (m_endDate > location.getEndDate())
    return false;

  if (m_intervalStartHour < location.getIntervalStartHour())
    return true;
  else if (m_intervalStartHour > location.getIntervalStartHour())
    return false;

  if (m_intervalEndHour < location.getIntervalEndHour())
    return true;
  else if (m_intervalEndHour > location.getIntervalEndHour())
    return false;

  if (m_nRepeats < location.getNRepeats())
    return true;
  else if (m_nRepeats > location.getNRepeats())
    return false;

  return (static_cast<size_t>(m_unit) < static_cast<size_t>(location.getRepeatUnit()));
}


} // namespace nac
} // namespace ndn
