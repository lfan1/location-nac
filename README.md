# Name-based Access Control for sharing spatial-temporal data

This is a fork from NAC (https://github.com/named-data/name-based-access-control), which supports spatio-temporal policies, and incorporates publish-subscribe functionality for real-time data sharing. The naming scheme follows technical report https://named-data.net/wp-content/uploads/2016/02/ndn-0034-2-nac.pdf
