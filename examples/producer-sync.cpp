#include <PSync/partial-producer.hpp>

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/util/logger.hpp>
#include <ndn-cxx/util/random.hpp>
#include <ndn-cxx/util/scheduler.hpp>

#include <ndn-cxx/security/key-chain.hpp>
#include <ndn-cxx/security/validator-config.hpp>

#include "encryptor.hpp"
#include "access-manager.hpp"

#include "version.hpp"

#include <boost/exception/get_error_info.hpp>
#include <ndn-cxx/util/logger.hpp>
#include <boost/asio.hpp>
#include <boost/exception/all.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <string>
#include <sstream> 
#include <chrono>
#include <thread>
#include <boost/filesystem.hpp>
#include "ndn-cxx/util/io.hpp"
#include <fstream>

#define PI 3.14159265


NDN_LOG_INIT(examples.ProducerApp);

namespace ndn {
namespace nac {
// Additional nested namespaces can be used to prevent/limit name conflicts
namespace examples {

static RsaKeyParams params;
using namespace boost::posix_time;
using namespace std;
using std::chrono::system_clock;

time::nanoseconds t_start;
time::nanoseconds t_sync;
time::nanoseconds t_sum;

// {35.120862, -89.936092, 60}, --- McWherter library
// {35.121185, -89.938107, 50}, --- Dunn Hall
// {35.121678, -89.940037, 100}, ---FIT

class ProducerSync : noncopyable
{

  struct dataInfo
  {
    Name dataName;
    EncryptedContent data;
  };

public:
  /**
   * @brief Initialize producer and schedule updates
   *
   * IBF size is set to 40 in m_producer as the expected number of update to IBF in a sync cycle
   */
  ProducerSync()
    : m_face(nullptr, m_keyChain)
    , m_producer(10, m_face, Name("/psync"), Name("/org/md2k/DATA/data"))
    , m_validator(m_face)
    , m_encryptor("/org/md2k/READ",
                  "/org/md2k/DATA/CK", 
                  signingWithSha256(),
                  "/tmp/minindn/data.db",
									"14000101T000000",
									3,
									Encryptor::RepeatUnit::SECOND,
                  {{35.120862, -89.936092, 100},{35.121185, -89.938107, 60}},
                  [] (auto...) {
                    std::cerr << "Failed to publish CK";
                  }, m_validator, m_keyChain, m_face)
    , m_scheduler(m_face.getIoService())
  {
    std::vector<std::vector<std::string>> datacsv = read_csv("/home/mini-ndn/Laqin/minindn_home/location_sec.csv");
		// t_start = time::toUnixTimestamp(time::system_clock::now());

    m_updateName = Name("/org/md2k/DATA/data");

    // std::cout << "updateName: " <<m_updateName << std::endl;
    
    // Add the user prefix to the producer
    m_producer.addUserNode(m_updateName);

    uint64_t x = 1;
    loopDataset(x, datacsv);
  
  }

  inline security::v2::Certificate
  loadCertificate(const std::string& fileName)
  {
    shared_ptr<security::v2::Certificate> cert;
    cert = io::load<security::v2::Certificate>(fileName);

    if (cert == nullptr) {
      BOOST_THROW_EXCEPTION(std::runtime_error("Cannot load certificate from " + fileName));
    }
    return *cert;
  }

  vector<string> 
  split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }

    return internal;
  }

  std::vector<std::vector<std::string>>
  read_csv(std::string filename){
      // Reads a CSV file into a vector of <string, vector<int>> pairs where
      // each pair represents <column name, column values>

      // Create a vector of <string, int vector> pairs to store the result
      std::vector<std::vector<std::string>> result;

      // Create an input filestream
      std::ifstream myFile(filename);

      // Make sure the file is open
      if(!myFile.is_open()) throw std::runtime_error("Could not open file");

      // Helper vars
      std::string line, colname;
      std::string val;

      // Read data, line by line
      while(std::getline(myFile, line))
      {

        std::vector<std::string> vec;
          // Create a stringstream of the current line
          std::stringstream ss(line);        
          // Keep track of the current column index
          int colIdx = 0;
          
          // Extract each integer
          while(std::getline(ss, val, ',')) {
              
              // Add the current integer to the 'colIdx' column's values vector
              vec.push_back(val);
              
              // If the next token is a comma, ignore it and move on
              if(ss.peek() == ',') ss.ignore();
              
          }
          result.push_back(vec);
      }

      // Close file
      myFile.close();

      return result;
  }


  void
  loopDataset(uint64_t index, std::vector<std::vector<std::string>> datacsv)
  {
    Name dataName("/org/md2k/");
    dataName.append("DATA").append("data");

    std::string content = "speed: "+ datacsv[index][4]+" bearing: " + datacsv[index][5]+ " accurary: "+datacsv[index][6] + " version: "+ datacsv[index][7]+" user: "+datacsv[index][8];
    double lat = std::stod(datacsv[index][1]);
    double lon = std::stod(datacsv[index][2]);

    time::system_clock::TimePoint timeslot;
    timeslot = time::fromString(datacsv[index][0]);
    dataName.append(datacsv[index][1]).append(datacsv[index][2]).append(time::toIsoString(timeslot));
    // index += 1

    NDN_LOG_INFO("data start generation " );
    m_encryptor.encrypt(timeslot, lat, lon, reinterpret_cast<const uint8_t*>(content.data()), content.size(),
    [=] (const auto& blob) {
        NDN_LOG_INFO("loop continue..." << index);

        NDN_LOG_INFO("data finish generation " );

        NDN_LOG_INFO("count of ck: " << m_encryptor.m_count);

        doUpdate(m_updateName);
        Name updateName = m_updateName;
        Name name = updateName.appendNumber(index);
        // std::cout << "updateName  " << name << std::endl;

        m_dataInfo[name].dataName = dataName;
        m_dataInfo[name].data = blob;

        m_scheduler.schedule(1_s, [=]() mutable{ 
                        index += 1; 
                        if (index < datacsv.size()) {
                        loopDataset(index, datacsv);
                         }});

      });

  }

  void
  run()
  {
    m_face.setInterestFilter("/org/md2k/DATA/data",
                             bind(&ProducerSync::onNameInterest, this, _1, _2),
                             RegisterPrefixSuccessCallback(),
                             bind(&ProducerSync::onRegisterFailed, this, _1, _2));  

    m_encryptor.getEncryptedCK();
    m_face.processEvents();
  }

private:

void
doUpdate(const ndn::Name& updateName)
{
  time::nanoseconds t_publish = time::toUnixTimestamp(time::system_clock::now());

  NDN_LOG_INFO("data publish " );
  // Publish an update to this user prefix
  m_producer.publishName(updateName);

  uint64_t seqNo =  m_producer.getSeqNo(updateName).value();

}

  static double 
toRadians(double degree)
{
    double rad;
    rad =  ( degree * PI ) / 180 ;
    return rad;
}

std::vector<double>
boundingCoordinates(double lat, double lon, double radius) {
    
  std::vector<double> result;
  double radDist = radius / 6371000.0;

  double minLatitude = toRadians(lat) - radDist;
  double maxLatitude = toRadians(lat) + radDist;
  double minLongitude;
  double maxLongitude;

  if (minLatitude > toRadians(-90.0) && maxLatitude < toRadians(90.0)) {
    double deltaLon = asin(sin(radDist) /cos(toRadians(lat)));
    minLongitude = toRadians(lon) - deltaLon;
    if (minLongitude < toRadians(-180.0)) minLongitude += 2 * PI;
    maxLongitude = toRadians(lon) + deltaLon;
    if (maxLongitude > toRadians(180.0)) maxLongitude -= 2 * PI;
  } else {
    // a pole is within the distance
    minLatitude = std::max(minLatitude, toRadians(-90.0));
    maxLatitude = std::min(maxLatitude, toRadians(90.0));
    minLongitude = toRadians(-180);
    maxLongitude = toRadians(180);
  }
	
	result.push_back(minLatitude);
	result.push_back(maxLatitude);
	result.push_back(minLongitude);
	result.push_back(maxLongitude);
  result.push_back(radDist);
	
	return result;
}

void
onNameInterest(const InterestFilter& filter, const Interest& interest)
{
  NDN_LOG_INFO("Producer on Name interest: " << interest.getName());

  // Create new name, based on Interest's name
  Name dataName(interest.getName());
  Data content;

  std::unordered_map<Name, dataInfo>::iterator dataIterator = m_dataInfo.find(dataName);
  
  if (dataIterator != m_dataInfo.end()) {

    content.setName(dataIterator->second.dataName);
    content.setFreshnessPeriod(10_s); // 1 seconds
    content.setContent(dataIterator->second.data.wireEncode());

    NDN_LOG_INFO("Producer data name: " << content.getName());
    
    m_keyChain.sign(content);

    // Create Data packet
    shared_ptr<Data> data = make_shared<Data>(dataName);
    data->setFreshnessPeriod(10_s); // 1 seconds

    data->setContent(content.wireEncode()); // in here, something wrong happens

    // Sign Data packet with default identity
    m_keyChain.sign(*data);

    // Return Data packet to the requester
    m_face.put(*data);

  }
  else {

    const string content = "No data";

    // Create Data packet
    auto data = make_shared<Data>(interest.getName());
    data->setFreshnessPeriod(10_s);

    data->setContentType(ndn::tlv::ContentType_Nack);
    data->setContent(reinterpret_cast<const uint8_t*>(content.data()), content.size());

    m_keyChain.sign(*data);

    // Return Data packet to the requester
    m_face.put(*data);

  } 

}

void
onRegisterFailed(const Name& prefix, const std::string& reason)
{
  std::cerr << "ERROR: Failed to register prefix \""
            << prefix << "\" in local hub's daemon (" << reason << ")"
            << std::endl;
  m_face.shutdown();
}

private:
  KeyChain m_keyChain;
  Face m_face;
  psync::PartialProducer m_producer;
  ValidatorConfig m_validator;

  Encryptor m_encryptor;
  std::unordered_map<Name, EncryptedContent> m_data;

  std::unordered_map<Name, dataInfo> m_dataInfo;
  std::unordered_map<Name, Name> m_name;

  ndn::Scheduler m_scheduler;

  Name m_updateName;
};

} // namespace examples
} // namespace nac
} // namespace ndn

std::string nac_helper = R"STR(
  help          Show all commands
  version       Show version and exit
  run           run directly
  add-member    Add schedule to the member
)STR";

int
main(int argc, char* argv[])
{
  try {
    ndn::nac::examples::ProducerSync producer;
    producer.run();
  }
  catch (const std::exception& e) {
    NDN_LOG_ERROR(e.what());
  }
}
