#include <PSync/consumer.hpp>

#include <ndn-cxx/name.hpp>
#include <ndn-cxx/util/logger.hpp>
#include <ndn-cxx/util/random.hpp>

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/validator-config.hpp>

// #include <ndn-nac/decryptor.hpp>
#include "decryptor.hpp"

#include <iostream>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <string>
#include <sstream> 
#include <boost/filesystem.hpp>
#include "ndn-cxx/util/io.hpp"
#include <boost/filesystem.hpp>
#include <fstream>

NDN_LOG_INIT(examples.Consumer1App);

// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
namespace ndn {
namespace nac {
// Additional nested namespaces can be used to prevent/limit name conflicts
namespace examples {

time::nanoseconds t_start;
time::nanoseconds t_de;

time::nanoseconds t1;
time::nanoseconds t_sync;
time::nanoseconds t_sum;
time::nanoseconds t_sum1;
time::nanoseconds t_sum2;
time::nanoseconds t_sum3;

class Consumer1 : noncopyable
{
public:
  /**
   * @brief Initialize consumer and start hello process
   *
   * 0.001 is the false positive probability of the bloom filter
   *
   * @param syncPrefix should be the same as producer
   * @param nSub number of subscriptions is used for the bloom filter (subscription list) size
   */
  Consumer1()
    : m_face(nullptr, m_keyChain)
    , m_consumer(Name("/psync"), m_face,
              std::bind(&Consumer1::afterReceiveHelloData, this, _1),
              std::bind(&Consumer1::processSyncUpdate, this, _1),
              10, 0.001, 1_s, 500_ms)
    , m_validator(m_face)
    // , m_decryptor(m_keyChain.createIdentity(Name("/localhost/operator-new"), RsaKeyParams()).getDefaultKey(), m_validator, m_keyChain, m_face)
    , m_decryptor(m_keyChain.getPib().getDefaultIdentity().getDefaultKey(), m_validator, m_keyChain, m_face)
    // , m_nSub(80)
    , m_rng(ndn::random::getRandomNumberEngine())
  {
    t1 = time::toUnixTimestamp(time::system_clock::now());

    m_validator.load(R"CONF(
      trust-anchor
      {
        type any
      }
    )CONF", "fake-config");
    // This starts the consumer side by sending a hello interest to the producer
    // When the producer responds with hello data, afterReceiveHelloData is called
    m_consumer.sendHelloInterest();

    std::cout << "sendHelloInterest: " << std::endl;
    NDN_LOG_INFO( "sendHelloInterest: ");
  }

  static const time::system_clock::TimePoint
  getCkEndTimeslot(const time::system_clock::TimePoint& timeslot, const time::milliseconds granuality)
  {
  // return time::fromUnixTimestamp((time::toUnixTimestamp(timeslot) / 3600000) * 3600000);
    return time::fromUnixTimestamp(time::toUnixTimestamp(timeslot) + granuality);
  }


  void
  run()
  {
    m_face.processEvents();
  }

private:
  // void
  // afterReceiveHelloData(const std::vector<ndn::Name>& availSubs)
  // {
  //   // Randomly subscribe to m_nSub prefixes
  //   std::vector<ndn::Name> sensors = availSubs;

  //   std::cout << "sensors size: " << sensors.size() << std::endl;

  //   std::shuffle(sensors.begin(), sensors.end(), m_rng);

  //   for (int i = 0; i < sensors.size(); i++) {
  //     NDN_LOG_INFO("Subscribing to: " << sensors[i]);
  //     std::cout << "Subscribing to: " << sensors[i] << std::endl;
  //     m_consumer.addSubscription(sensors[i]);
  //   }

  //   t_sync = time::toUnixTimestamp(time::system_clock::now());

  //   // After setting the subscription list, send the sync interest
  //   // The sync interest contains the subscription list
  //   // When new data is received for any subscribed prefix, processSyncUpdate is called
  //   m_consumer.sendSyncInterest();
  // }

  void
  afterReceiveHelloData(const std::map<ndn::Name, uint64_t>& availSubs)
  {
    // Randomly subscribe to m_nSub prefixes
    std::vector<ndn::Name> sensors;

    std::cout << "sensors size: " << sensors.size() << std::endl;

    sensors.reserve(availSubs.size());
    for (const auto& it : availSubs) {
    sensors.insert(sensors.end(), it.first);
    }

    // std::shuffle(sensors.begin(), sensors.end(), m_rng);

    for (int i = 0; i < sensors.size(); i++) {
    NDN_LOG_INFO("Subscribing to: " << sensors[i]);
    std::cout << "Subscribing to: " << sensors[i] << std::endl;

    auto it = availSubs.find(sensors[i]);
    m_consumer.addSubscription(sensors[i], it->second);
    }

    // t_sync = time::toUnixTimestamp(time::system_clock::now());

    // After setting the subscription list, send the sync interest
    // The sync interest contains the subscription list
    // When new data is received for any subscribed prefix, processSyncUpdate is called
    m_consumer.sendSyncInterest();
  }

  void
  processSyncUpdate(const std::vector<psync::MissingDataInfo>& updates)
  {
      time::nanoseconds t2 = time::toUnixTimestamp(time::system_clock::now());

      NDN_LOG_INFO("sync data update ");

      NDN_LOG_INFO( "sync call back: " <<t2);

    for (const auto& update : updates) {

      NDN_LOG_INFO( "processSyncUpdate: " << update.prefix << " "<< update.lowSeq << " "<< update.highSeq );

      for (uint64_t i = update.lowSeq; i <= update.highSeq; i++) {
        // Data can now be fetched using the prefix and sequence
        NDN_LOG_INFO("Update: " << update.prefix << "/" << i);

        ndn::Name interestName = update.prefix;
        interestName = interestName.appendNumber(i);
        NDN_LOG_INFO("Fetching data for prefix:" << interestName);
        expressInterest(interestName);
      }
    }
  }

  void
  expressInterest(const ndn::Name& name)
  {
    NDN_LOG_INFO( "consumer express interest for data " << name );
    ndn::Interest interest(name);
    interest.setMustBeFresh(true);
    interest.setInterestLifetime(2_s);

    m_face.expressInterest(interest,
                          bind(&Consumer1::onData, this,  _1, _2),
                          bind(&Consumer1::onNack, this, _1, _2),
                          bind(&Consumer1::onTimeout, this, _1));


  }

  void
  onData(const Interest& interest, const Data& data)
  {
    time::nanoseconds te = time::toUnixTimestamp(time::system_clock::now());

    NDN_LOG_INFO( "consumer data call back ");

    if (data.getMetaInfo().getType() == ndn::tlv::ContentType_Nack) {

      const uint8_t* bytes = data.getContent().value();
      const int len = data.getContent().value_size();

      std::string s(reinterpret_cast<char const*>(bytes), len);
      std::cout << "received Nack with reason: " << s
              << " for interest " << interest << std::endl;
      return;
    }

    auto c = data.getContent();

    try {
      c.parse();
    }
    catch (const ndn::tlv::Error& e) {
      std::cout << " Can not parse the response " << e.what();
    }

    shared_ptr<Data> d = make_shared<Data>(c.get(ndn::tlv::Data));

    // std::cout << "receive data name : " << boost::lexical_cast<std::string>(*d);
    
    t_de = time::toUnixTimestamp(time::system_clock::now());

    NDN_LOG_INFO( "consumer start decryption ");

    m_validator.validate(*d,
      [=] (const Data& data) {
        m_decryptor.decrypt(d->getContent().blockFromValue(),
          [=] (ConstBufferPtr content) {

            time::nanoseconds te1 = time::toUnixTimestamp(time::system_clock::now());
	
            NDN_LOG_INFO( "consumer finish decryption" );

            NDN_LOG_INFO( "Decrypted content: " << std::string(reinterpret_cast<const char*>(content->data()), content->size()));

          },
          [=] (const ErrorCode&, const std::string& error) {
            std::cerr << "Cannot decrypt data: " << error << std::endl;
          });
      },
      [=] (const Data& data, const ValidationError& error) {
        std::cerr << "Cannot validate retrieved data: " << error << std::endl;
      });
  }

  void
  onNack(const Interest& interest, const lp::Nack& nack)
  {
    std::cout << "received Nack with reason " << nack.getReason()
              << " for interest " << interest << std::endl;
  }

  void
  onTimeout(const Interest& interest)
  {
    std::cout << "Timeout " << interest << std::endl;
  }

private:
  KeyChain m_keyChain;
  Face m_face;
  psync::Consumer m_consumer;
  ValidatorConfig m_validator;
  Decryptor m_decryptor;

  int m_nSub;

  ndn::random::RandomNumberEngine& m_rng;
};
} // namespace examples
} // namespace nac
} // namespace ndn

int
main(int argc, char* argv[])
{
  // if (argc != 3) {
  //   std::cout << "usage: " << argv[0] << " "
  //             << "<sync-prefix> <number-of-subscriptions>" << std::endl;
  //   return 1;
  // }

  try {
    ndn::nac::examples::Consumer1 consumer;
    consumer.run();
  }
  catch (const std::exception& e) {
    NDN_LOG_ERROR(e.what());
  }
}
