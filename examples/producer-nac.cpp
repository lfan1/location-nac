/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * NAC library is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * NAC library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and GNU Lesser
 * General Public License along with ndn-cxx, e.g., in COPYING.md file.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * See AUTHORS.md for complete list of NAC library authors and contributors.
 */

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include <ndn-cxx/security/validator-config.hpp>

#include "encryptor.hpp"
#include "access-manager.hpp"

// #include "tools/ndn-nac.hpp"
#include "version.hpp"

#include <boost/exception/get_error_info.hpp>
#include <ndn-cxx/util/logger.hpp>
#include <boost/asio.hpp>
#include <boost/exception/all.hpp>
#include <boost/program_options/options_description.hpp>
#include <boost/program_options/parsers.hpp>
#include <boost/program_options/variables_map.hpp>

#include <iostream>
#include <chrono>
#include <thread>
#include <fstream>
#include <vector>
#include <set>
#include <string>
#include <sstream> 
#include <boost/filesystem.hpp>
#include "ndn-cxx/util/io.hpp"
#include <boost/filesystem.hpp>

#define PI 3.14159265
NDN_LOG_INIT(examples.ProducerNACApp);


// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
namespace ndn {
namespace nac {
// Additional nested namespaces can be used to prevent/limit name conflicts
namespace examples {

static RsaKeyParams params;
using namespace boost::posix_time;
using namespace std;

time::nanoseconds t_start;

// {35.120862, -89.936092, 60}, --- McWherter library
// {35.121185, -89.938107, 50}, --- Dunn Hall
// {35.121678, -89.940037, 100}, ---FIT

class ProducerNac : noncopyable
{
public:
  ProducerNac()
    : m_face(nullptr, m_keyChain)
    , m_validator(m_face)
    , m_encryptor("/org/md2k/READ",
                  "/org/md2k/DATA/CK", 
                  signingWithSha256(),
                  "/tmp/minindn/data.db",
									"14000101T000000",
									1,
									Encryptor::RepeatUnit::HOUR,
                  {{35.120862, -89.936092, 100},{35.121185, -89.938107, 60}},
                  [] (auto...) {
                    std::cerr << "Failed to publish CK";
                  }, m_validator, m_keyChain, m_face)\
  {
    //   std::cout << "Start....." << std::endl;
    m_validator.load(R"CONF(
        trust-anchor
        {
          type any
        }
      )CONF", "fake-config");
  }

  std::vector<std::vector<std::string>>
  read_csv(std::string filename){
      // Create a vector of <string, int vector> pairs to store the result
      std::vector<std::vector<std::string>> result;
      // Create an input filestream
      std::ifstream myFile(filename);
      // Make sure the file is open
      if(!myFile.is_open()) throw std::runtime_error("Could not open file");

      std::string line, colname;
      std::string val;

      // Read data, line by line
      while(std::getline(myFile, line))
      {
        std::vector<std::string> vec;
          // Create a stringstream of the current line
          std::stringstream ss(line);        
          
          // Extract each integer
          while(std::getline(ss, val, ',')) {  
              // Add the current integer to vector
              vec.push_back(val);           
              // If the next token is a comma, ignore it and move on
              if(ss.peek() == ',') ss.ignore();             
          }
          result.push_back(vec);
      }

      // Close file
      myFile.close();
      return result;
  }

  void
  loopDataset(size_t index, std::vector<std::vector<std::string>> datacsv)
  {
    Name dataName("/org/md2k/");
    dataName.append("DATA").append("data");

    std::string content = "speed: "+ datacsv[index][4]+" bearing: " + datacsv[index][5]+ " accurary: "+datacsv[index][6] + " version: "+ datacsv[index][7]+" user: "+datacsv[index][8];
    double lat = std::stod(datacsv[index][1]);
    double lon = std::stod(datacsv[index][2]);

    time::system_clock::TimePoint timeslot;
    timeslot = time::fromString(datacsv[index][0]);
    dataName.append(datacsv[index][1]).append(datacsv[index][2]).append(time::toIsoString(timeslot));

    std::cout << "dataName..." << dataName << std::endl;
    index += 1;
   
    NDN_LOG_INFO("data start generation " );

    m_encryptor.encrypt(timeslot, lat, lon, reinterpret_cast<const uint8_t*>(content.data()), content.size(),
    [=] (const auto& blob) {
      
      NDN_LOG_INFO("data finish generation " );

      std::cout << "loop continue..." << index << std::endl;
      m_data[dataName] = blob;
      if (index < datacsv.size()) {
        loopDataset(index, datacsv);
      }

    });

    NDN_LOG_INFO("data done with encryption" );

    NDN_LOG_INFO("count of ck: " << m_encryptor.m_count);

  }

  void
  run()
  {    
    std::vector<std::vector<std::string>> datacsv = read_csv("/tmp/minindn/location_sec.csv");

//    NDN_LOG_INFO("data start encryption " );

    size_t x = 1;
    loopDataset(x, datacsv);

   // NDN_LOG_INFO("data finish encryption " );

    //NDN_LOG_INFO("count of ck: " << m_encryptor.m_count);
              
    m_face.setInterestFilter("/org/md2k/DATA/data",
                             bind(&ProducerNac::onInterest, this, _1, _2),
                             RegisterPrefixSuccessCallback(),
                             bind(&ProducerNac::onRegisterFailed, this, _1, _2));  

    m_encryptor.getEncryptedCK();

    m_face.processEvents();
}

private:

static double 
toRadians(double degree)
{
    double rad;
    rad =  ( degree * PI ) / 180 ;
    return rad;
}

std::vector<double>
boundingCoordinates(double lat, double lon, double radius) {
    
  std::vector<double> result;
  double radDist = radius / 6371000.0;

  double minLatitude = toRadians(lat) - radDist;
  double maxLatitude = toRadians(lat) + radDist;
  double minLongitude;
  double maxLongitude;

  if (minLatitude > toRadians(-90.0) && maxLatitude < toRadians(90.0)) {
    double deltaLon = asin(sin(radDist) /cos(toRadians(lat)));
    minLongitude = toRadians(lon) - deltaLon;
    if (minLongitude < toRadians(-180.0)) minLongitude += 2 * PI;
    maxLongitude = toRadians(lon) + deltaLon;
    if (maxLongitude > toRadians(180.0)) maxLongitude -= 2 * PI;
  } else {
    // a pole is within the distance
    minLatitude = std::max(minLatitude, toRadians(-90.0));
    maxLatitude = std::min(maxLatitude, toRadians(90.0));
    minLongitude = toRadians(-180);
    maxLongitude = toRadians(180);
  }
	
	result.push_back(minLatitude);
	result.push_back(maxLatitude);
	result.push_back(minLongitude);
	result.push_back(maxLongitude);
  result.push_back(radDist);
	
	return result;
}

  void
  onInterest(const InterestFilter& filter, const Interest& interest)
  {
    std::cout << "Producer on interest: " << interest.getName() << std::endl;
    NDN_LOG_INFO("Producer on interest: " << interest.getName());

    // Create new name, based on Interest's name
    Name dataName(interest.getName());

		double lat = std::stod(interest.getName().get(-3).toUri());
    double lon = std::stod(interest.getName().get(-2).toUri());
    double rad= std::stod(interest.getName().get(-1).toUri());

		std::string timeslot = interest.getName().get(-4).toUri();

		EncryptedContent content;

    std::vector<double> re = boundingCoordinates(lat, lon, rad);

    std::unordered_map<Name, EncryptedContent>::iterator it;
    for (it = m_data.begin(); it != m_data.end(); ++it) {

			double latitude = std::stod(it->first.get(-3).toUri());
    	double longitude = std::stod(it->first.get(-2).toUri());
			std::string ts = it->first.get(-1).toUri();

			double r = acos(sin(toRadians(lat))*sin(toRadians(latitude)) + cos(toRadians(lat))*cos(toRadians(latitude))*cos(toRadians(longitude) - (toRadians(lon))));
			if (toRadians(latitude) >= re[0] && toRadians(latitude) <= re[1] && toRadians(longitude) >= re[2] && toRadians(longitude) <= re[3] && r <= re[4] 
          && ts == timeslot) 
			{
				content = it->second;
				break;
			}

		}

		std::cout << "Producer find out content: " << content.hasKeyLocator() << std::endl;

    if (content.hasKeyLocator()) {
      // Create Data packet
      shared_ptr<Data> data = make_shared<Data>();
      data->setName(dataName);
      data->setFreshnessPeriod(10_s); // 10 seconds

      data->setContent(content.wireEncode());

      // Sign Data packet with default identity
      m_keyChain.sign(*data);

      // Return Data packet to the requester
      m_face.put(*data);
      std::cout << "Producer sends out data for interest: " << interest << std::endl;

    }
    else {

      std::cout << "Producer sends out NACk " << interest << std::endl;

      const string content = "No data";

    // Create Data packet
    auto data = make_shared<Data>(interest.getName());
    data->setFreshnessPeriod(10_s);

    data->setContentType(ndn::tlv::ContentType_Nack);
    data->setContent(reinterpret_cast<const uint8_t*>(content.data()), content.size());


      // Sign Data packet with default identity
      m_keyChain.sign(*data);
      // m_keyChain.sign(data, <identityName>);
      // m_keyChain.sign(data, <certificate>);

      // Return Data packet to the requester
      m_face.put(*data);
      std::cout << "Producer sends out data for interest: " << interest << std::endl;

    }

  }

  void
  onRegisterFailed(const Name& prefix, const std::string& reason)
  {
    std::cerr << "ERROR: Failed to register prefix \""
              << prefix << "\" in local hub's daemon (" << reason << ")"
              << std::endl;
    m_face.shutdown();
  }

private:
  KeyChain m_keyChain;
  Face m_face;
  ValidatorConfig m_validator;

  Encryptor m_encryptor;
	std::unordered_map<Name, EncryptedContent> m_data;
};

} // namespace examples
} // namespace nac
} // namespace ndn

std::string nac_helper = R"STR(
  help          Show all commands
  version       Show version and exit
  run           run directly
  add-member    Add schedule to the member
)STR";

int
main(int argc, char** argv)
{
  ndn::nac::examples::ProducerNac producer;
  try {
    producer.run();
  }
  catch (const std::exception& e) {
    std::cerr << "ERROR: " << e.what() << std::endl;
  }
  return 0;
}
