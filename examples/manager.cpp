/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * NAC library is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * NAC library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and GNU Lesser
 * General Public License along with ndn-cxx, e.g., in COPYING.md file.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * See AUTHORS.md for complete list of NAC library authors and contributors.
 */

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/key-chain.hpp>
#include <ndn-cxx/security/validator-config.hpp>
#include <ndn-cxx/util/logger.hpp>
#include <ndn-cxx/util/random.hpp>

#include "encryptor.hpp"
#include "access-manager.hpp"

#include <iostream>
#include <boost/filesystem.hpp>
#include "ndn-cxx/util/io.hpp"

NDN_LOG_INIT(examples.ManagerApp);

// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
namespace ndn {
namespace nac {
// Additional nested namespaces can be used to prevent/limit name conflicts
namespace examples {


static RsaKeyParams params;

using namespace boost::posix_time;
using namespace std;

class Manager : noncopyable
{
public:
  Manager()
    : m_face(nullptr, m_keyChain)
    , m_validator(m_face)
    , m_accessManager(m_keyChain.createIdentity("/org/md2k", RsaKeyParams()), "test",
                      m_keyChain, m_face,
                      "/home/mini-ndn/Laqin/minindn_home/data.db",
                      RsaKeyParams(), 1)
  {
    m_validator.load(R"CONF(
        trust-anchor
        {
          type any
        }
      )CONF", "fake-config");
  }

  // inline security::v2::Certificate
  // loadCertificate(const std::string& fileName)
  // {
  //   shared_ptr<security::v2::Certificate> cert;
  //   cert = io::load<security::v2::Certificate>(fileName);

  //   std::cout << "loadCertificate from: "<< fileName <<" " << cert<< std::endl;

  //   if (cert == nullptr) {
  //     BOOST_THROW_EXCEPTION(std::runtime_error("Cannot load certificate from " + fileName));
  //   }
  //   return *cert;
  // }

  inline security::v2::Certificate
  loadCertificate(const std::string& fileName)
  {
    shared_ptr<security::v2::Certificate> cert;
    auto safebag = io::load<security::SafeBag>(fileName);
    auto data = safebag->getCertificate();

    std::cout << "loadCertificate from: "<< fileName <<" " << std::endl;
     NDN_LOG_INFO("loadCertificate from: "<< fileName);

    cert = make_shared<security::v2::Certificate>(data);

    if (cert == nullptr) {
      BOOST_THROW_EXCEPTION(std::runtime_error("Cannot load certificate from " + fileName));
    }
    return *cert;
  }

  vector<string> 
  split(string str, char delimiter) {
    vector<string> internal;
    stringstream ss(str); // Turn the string into a stream.
    string tok;

    while(getline(ss, tok, delimiter)) {
        internal.push_back(tok);
    }

    return internal;
  }

  void
  nac_add_member(const std::string& scheduleMember)
  {
    std::string schedule;
    std::string member;

    vector<string> sep = split(scheduleMember, ' ');
    std::cout << "schedule member: "<< sep[0] <<" " << sep[1]<< std::endl;
    schedule = sep[0];
    member = sep[1];

    try {
        auto cert = loadCertificate(member);
        std::cout << "cert: "<< cert.getKeyName() << std::endl; 
        std::ifstream file(schedule);
        std::string str;
        Schedule s;

        while (std::getline(file, str)) {
            vector<string> sep1 = split(str, ' ');
            std::string s1 = sep1[0];
            std::string s2 = sep1[1];
            size_t s3;
            std::stringstream sstream1(sep1[2]);
            sstream1 >> s3;
            size_t s4;
            std::stringstream sstream2(sep1[3]);
            sstream2 >> s4;
            size_t s5;
            std::stringstream sstream3(sep1[4]);
            sstream3 >> s5;
            std::string s6 = sep1[5];
            std::string s7 = sep1[6];

            Interval::RepeatUnit unit;

            if (s6 == "DAY") {
                unit = Interval::RepeatUnit::DAY;

            } else if (s6 == "MONTH") {
                unit = Interval::RepeatUnit::MONTH;

            } else if (s6 == "YEAR") {
                unit = Interval::RepeatUnit::YEAR;

            } else {
                unit = Interval::RepeatUnit::NONE;

            }

            Interval interval(from_iso_string(s1),
                                        from_iso_string(s2),
                                        s3,
                                        s4,
                                        s5,
                                        unit);

            if (s7 == "Allow") {
                s.addWhiteInterval(interval);
            } else {
                s.addBlackInterval(interval);
            }   
        }

        std::string scheduleName = boost::filesystem::path(schedule).stem().string();
        // add to the group manager db
        m_accessManager.addSchedule(scheduleName, s);

        // add members to the database
        m_accessManager.addMember(scheduleName, cert);
    }
    catch (const std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }

  }

  void
  nac_add_member_location(const std::string& scheduleMember)
  {
    std::string locationlist;
    std::string member;

    vector<string> sep = split(scheduleMember, ' ');
    std::cout << "locationlist and member: "<< sep[0] << " " << sep[1]<< std::endl;
    locationlist = sep[0];
    member = sep[1];

    try {
        auto cert = loadCertificate(member);

        // cert.setName(Name("/ndn/memberB/KEY/").appendVersion().append("/123")); //for demo on the same computer

        std::cout << "cert: "<< cert.getName() << std::endl; 
        std::ifstream file(locationlist);
        std::string str;
        LocationList l;

        while (std::getline(file, str)) {
            vector<string> sep1 = split(str, ' ');
            std::string s1 = sep1[0];
            std::string s2 = sep1[1];
            std::string s3 = sep1[2];
            std::string s4 = sep1[3];

            Location location(std::stod(s1), std::stod(s2), std::stod(s3));

            if (s4 == "Allow") {
                l.addWhiteLocation(location);
            } else {
                l.addBlackLocation(location);
            }   
        }

        std::string scheduleName = boost::filesystem::path(locationlist).stem().string();
        // add to the group manager db
        m_accessManager.addLocationList(scheduleName, l);

        // add members to the database
        m_accessManager.addMemberLocation(scheduleName, cert);
    }
    catch (const std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }

  }

  void
  nac_add_member_location_time(const std::string& scheduleMember)
  {
    std::string schedule;
    std::string member;

    vector<string> sep = split(scheduleMember, ' ');
    std::cout << "access-policy and member: "<< sep[0] << " " << sep[1]<< std::endl;
    schedule = sep[0];
    member = sep[1];

    try {
        auto cert = loadCertificate(member);

        // cert.setName(Name("/ndn/memberB/KEY/").appendVersion().append("/123")); //for demo on the same computer

        std::cout << "cert: "<< cert.getName() << std::endl; 
        std::ifstream file(schedule);
        std::string str;

        TimeLocationList l;

        while (std::getline(file, str)) {
          
            vector<string> sep1 = split(str, ' ');
            std::string s1 = sep1[0];
            std::string s2 = sep1[1];
            size_t s3;
            std::stringstream sstream1(sep1[2]);
            sstream1 >> s3;
            size_t s4;
            std::stringstream sstream2(sep1[3]);
            sstream2 >> s4;
            size_t s5;
            std::stringstream sstream3(sep1[4]);
            sstream3 >> s5;
            std::string s6 = sep1[5];
    
            std::string s7 = sep1[6];
            std::string s8 = sep1[7];
            std::string s9 = sep1[8];
            std::string s10 = sep1[9];

            TimeLocation::RepeatUnit unit;
            if (s6 == "DAY") {
                unit = TimeLocation::RepeatUnit::DAY;

            } else if (s6 == "MONTH") {
                unit = TimeLocation::RepeatUnit::MONTH;

            } else if (s6 == "YEAR") {
                unit = TimeLocation::RepeatUnit::YEAR;

            } else {
                unit = TimeLocation::RepeatUnit::NONE;

            }
            
            TimeLocation location(std::stod(s7), 
                                  std::stod(s8), 
                                  std::stod(s9), 
                                  from_iso_string(s1),
                                  from_iso_string(s2),
                                  s3,
                                  s4,
                                  s5,
                                  unit);

            if (s10 == "Allow") {
                l.addWhiteTimeLocation(location);
            } else {
                l.addBlackTimeLocation(location);
            }   
        }

        std::string scheduleName = boost::filesystem::path(schedule).stem().string();

         // add timelocationlist to db
        m_accessManager.addTimeLocationList(scheduleName, l);

        m_accessManager.addMemberTimeLocation(scheduleName, cert);

    }
    catch (const std::exception& e) {
        std::cerr << "ERROR: " << e.what() << std::endl;
    }

  }

  void
  run()
  {
    // give access to default identity. If consumer uses the same default identity, he will be able to decrypt
    // auto cert = m_keyChain.getPib().getDefaultIdentity().getDefaultKey().getDefaultCertificate();

    // configure start-time and end-time for policies
    std::string starttime = "20200525T000100";
    std::string endTime = "20200529T000000";
    TimeStamp sTimeslot = boost::posix_time::from_iso_string(starttime);
    TimeStamp eTimeslot = boost::posix_time::from_iso_string(endTime);
    // TimeStamp sTimeslot;

    // configure location list
    std::vector< std::vector<double>>  locations;
    locations = {{35.120862, -89.936092, 100},{35.121185, -89.938107, 60}};
    time::nanoseconds t_start = time::toUnixTimestamp(time::system_clock::now());
    m_accessManager.getGroupKey(sTimeslot, eTimeslot, locations);

    std::cout <<"key size: " << m_accessManager.getKeklist().size()<< std::endl;

    NDN_LOG_INFO("key size: " << m_accessManager.getKeklist().size());
    time::nanoseconds t_end = time::toUnixTimestamp(time::system_clock::now());
		std::cout << "KEK/KDK generarion time: " <<(t_end - t_start)<< std::endl;
    NDN_LOG_INFO("KEK/KDK generarion time: " <<(t_end - t_start));
  
    m_face.setInterestFilter("/org/md2k/READ/KEK",
                             bind(&Manager::onInterest, this, _1, _2),
                             RegisterPrefixSuccessCallback(),
                             bind(&Manager::onRegisterFailed, this, _1, _2));

    m_accessManager.getKdk();

    time::nanoseconds t_end1 = time::toUnixTimestamp(time::system_clock::now());
	  std::cout << "KEK/KDK publishing time: " <<(t_end1 - t_end)<< std::endl;

    m_face.processEvents();
  }

private:
  void
  onInterest(const InterestFilter& filter, const Interest& interest)
  {
    time::nanoseconds ts = time::toUnixTimestamp(time::system_clock::now());
    std::cout <<"\n" << std::endl;
    std::cout << "Manager generate KekData for time-location nac: " << interest.getName() << ", need to generate one!" << std::endl;
    
    std::string time = interest.getName().get(-4).toUri();
    std::string lat = interest.getName().get(-3).toUri();
    std::string lon = interest.getName().get(-2).toUri();
    std::string rad = interest.getName().get(-1).toUri();

    time::system_clock::TimePoint timeslot;
    timeslot = time::fromIsoString(time);

    std::list<Data> keys =  m_accessManager.getKeklist();

    std::list<Data>::iterator it;
    for (it = keys.begin(); it != keys.end(); ++it) {

      Name keyName = it->getName();

      if ( lat == keyName.get(-4).toUri() && lon == keyName.get(-3).toUri() && rad == keyName.get(-2).toUri()
            && timeslot >= time::fromIsoString(keyName.get(-6).toUri())&& timeslot < time::fromIsoString(keyName.get(-5).toUri()))
        {
          shared_ptr<Data> ekey = make_shared<Data>(*it);

          Name subName = keyName.getSubName(Name(m_accessManager.m_nacKey.getIdentity()).size());
          Name interestName = interest.getName();

          Name dataName = interestName.append(subName);
          // Name keyName = dataName.append(KEK_Time_Location).append(name.get(-6)).append(name.get(-5)).append(name.get(-4)).append(name.get(-3)).append(name.get(-2)).append(name.get(-1));
          ekey->setName(dataName);
          m_face.put(*ekey);
          std::cout << "Manager sends out KEK data for TLNAC: " << keyName << std::endl;
          std::cout <<"\n" << std::endl;

          break;
        } 
    }

    time::nanoseconds te = time::toUnixTimestamp(time::system_clock::now());
    std::cout << "KEK publishing time: " <<(te - ts)<< std::endl;
  }

  void
  onRegisterFailed(const Name& prefix, const std::string& reason)
  {
    std::cerr << "ERROR: Failed to register prefix \""
              << prefix << "\" in local hub's daemon (" << reason << ")"
              << std::endl;
    m_face.shutdown();
  }

private:
  KeyChain m_keyChain;
  Face m_face;
  ValidatorConfig m_validator;
  AccessManager m_accessManager;
};

} // namespace examples
} // namespace nac
} // namespace ndn

std::string nac_helper = R"STR(
  help          Show all commands
  version       Show version and exit
  run           run directly
  add-member    Add schedule to the member
)STR";

int
main(int argc, char** argv)
{
  if (argc < 2) {
    std::cerr << nac_helper << std::endl;
    return 1;
  }

  ndn::nac::examples::Manager manager;

  using namespace ndn::nac;

  std::string command(argv[1]);
  try {
    if (command == "help")              { std::cout << nac_helper << std::endl; }
    else if (command == "version")      { std::cout << "Version" << std::endl; }
    else if (command == "run")   {
        std::cout << "directly run" << std::endl;
        try {
            manager.run();
        }
        catch (const std::exception& e) {
            std::cerr << "ERROR: " << e.what() << std::endl;
        }
        return 0;
        }
    else if (command == "add-member") {

        int test = 1;
        std::string scheduleMember;

        if (test == 1) {
        std::string scheduleMember1 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c1/c1.ndnkey";
        std::string scheduleMember2 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c2/c2.ndnkey";
        std::string scheduleMember3 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c3/c3.ndnkey";
        std::string scheduleMember4 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c4/c4.ndnkey";
        std::string scheduleMember5 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c5/c5.ndnkey";
        std::string scheduleMember6 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c6/c6.ndnkey";
        std::string scheduleMember7 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c7/c7.ndnkey";
        std::string scheduleMember8 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c8/c8.ndnkey";
        std::string scheduleMember9 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c9/c9.ndnkey";
        std::string scheduleMember10 = "/home/mini-ndn/Laqin/minindn_home/time-location1.txt /home/mini-ndn/Laqin/minindn_home/c10/c10.ndnkey";

        manager.nac_add_member_location_time(scheduleMember1); 
        manager.nac_add_member_location_time(scheduleMember2);
        manager.nac_add_member_location_time(scheduleMember3); 
        manager.nac_add_member_location_time(scheduleMember4); 
        manager.nac_add_member_location_time(scheduleMember5); 
        manager.nac_add_member_location_time(scheduleMember6); 
        manager.nac_add_member_location_time(scheduleMember7); 
        manager.nac_add_member_location_time(scheduleMember8); 
        manager.nac_add_member_location_time(scheduleMember9); 
        manager.nac_add_member_location_time(scheduleMember10); 

        }
        else {

          std::string scheduleMember1 = "/tmp/minindn/time-location1.txt /tmp/minindn/c1/c1.ndnkey";
          std::string scheduleMember2 = "/tmp/minindn/time-location1.txt /tmp/minindn/c2/c2.ndnkey";
          std::string scheduleMember3 = "/tmp/minindn/time-location1.txt /tmp/minindn/c3/c3.ndnkey";
          std::string scheduleMember4 = "/tmp/minindn/time-location1.txt /tmp/minindn/c4/c4.ndnkey";
          std::string scheduleMember5 = "/tmp/minindn/time-location1.txt /tmp/minindn/c5/c5.ndnkey";
          std::string scheduleMember6 = "/tmp/minindn/time-location1.txt /tmp/minindn/c6/c6.ndnkey";
          std::string scheduleMember7 = "/tmp/minindn/time-location1.txt /tmp/minindn/c7/c7.ndnkey";
          std::string scheduleMember8 = "/tmp/minindn/time-location1.txt /tmp/minindn/c8/c8.ndnkey";
          std::string scheduleMember9 = "/tmp/minindn/time-location1.txt /tmp/minindn/c9/c9.ndnkey";
          std::string scheduleMember10 = "/tmp/minindn/time-location1.txt /tmp/minindn/c10/c10.ndnkey";

       std::string scheduleMember11 = "/tmp/minindn/time-location1.txt /tmp/minindn/c11/c11.ndnkey";
       std::string scheduleMember12 = "/tmp/minindn/time-location1.txt /tmp/minindn/c12/c12.ndnkey";
       std::string scheduleMember13 = "/tmp/minindn/time-location1.txt /tmp/minindn/c13/c13.ndnkey";
       std::string scheduleMember14 = "/tmp/minindn/time-location1.txt /tmp/minindn/c14/c14.ndnkey";
       std::string scheduleMember15 = "/tmp/minindn/time-location1.txt /tmp/minindn/c15/c15.ndnkey";
       std::string scheduleMember16 = "/tmp/minindn/time-location1.txt /tmp/minindn/c16/c16.ndnkey";
       std::string scheduleMember17 = "/tmp/minindn/time-location1.txt /tmp/minindn/c17/c17.ndnkey";
       std::string scheduleMember18 = "/tmp/minindn/time-location1.txt /tmp/minindn/c18/c18.ndnkey";
       std::string scheduleMember19 = "/tmp/minindn/time-location1.txt /tmp/minindn/c19/c19.ndnkey";
       std::string scheduleMember20 = "/tmp/minindn/time-location1.txt /tmp/minindn/c20/c20.ndnkey";

       std::string scheduleMember21 = "/tmp/minindn/time-location1.txt /tmp/minindn/c21/c21.ndnkey";
       std::string scheduleMember22 = "/tmp/minindn/time-location1.txt /tmp/minindn/c22/c22.ndnkey";
       std::string scheduleMember23 = "/tmp/minindn/time-location1.txt /tmp/minindn/c23/c23.ndnkey";
       std::string scheduleMember24 = "/tmp/minindn/time-location1.txt /tmp/minindn/c24/c24.ndnkey";
       std::string scheduleMember25 = "/tmp/minindn/time-location1.txt /tmp/minindn/c25/c25.ndnkey";
       std::string scheduleMember26 = "/tmp/minindn/time-location1.txt /tmp/minindn/c26/c26.ndnkey";
       std::string scheduleMember27 = "/tmp/minindn/time-location1.txt /tmp/minindn/c27/c27.ndnkey";
       std::string scheduleMember28 = "/tmp/minindn/time-location1.txt /tmp/minindn/c28/c28.ndnkey";
       std::string scheduleMember29 = "/tmp/minindn/time-location1.txt /tmp/minindn/c29/c29.ndnkey";
       std::string scheduleMember30 = "/tmp/minindn/time-location1.txt /tmp/minindn/c30/c30.ndnkey";

       std::string scheduleMember31 = "/tmp/minindn/time-location1.txt /tmp/minindn/c31/c31.ndnkey";
       std::string scheduleMember32 = "/tmp/minindn/time-location1.txt /tmp/minindn/c32/c32.ndnkey";
       std::string scheduleMember33 = "/tmp/minindn/time-location1.txt /tmp/minindn/c33/c33.ndnkey";
       std::string scheduleMember34 = "/tmp/minindn/time-location1.txt /tmp/minindn/c34/c34.ndnkey";
       std::string scheduleMember35 = "/tmp/minindn/time-location1.txt /tmp/minindn/c35/c35.ndnkey";
       std::string scheduleMember36 = "/tmp/minindn/time-location1.txt /tmp/minindn/c36/c36.ndnkey";
       std::string scheduleMember37 = "/tmp/minindn/time-location1.txt /tmp/minindn/c37/c37.ndnkey";
       std::string scheduleMember38 = "/tmp/minindn/time-location1.txt /tmp/minindn/c38/c38.ndnkey";
       std::string scheduleMember39 = "/tmp/minindn/time-location1.txt /tmp/minindn/c39/c39.ndnkey";
       std::string scheduleMember40 = "/tmp/minindn/time-location1.txt /tmp/minindn/c40/c40.ndnkey";

       std::string scheduleMember41 = "/tmp/minindn/time-location1.txt /tmp/minindn/c41/c41.ndnkey";
       std::string scheduleMember42 = "/tmp/minindn/time-location1.txt /tmp/minindn/c42/c42.ndnkey";
       std::string scheduleMember43 = "/tmp/minindn/time-location1.txt /tmp/minindn/c43/c43.ndnkey";
       std::string scheduleMember44 = "/tmp/minindn/time-location1.txt /tmp/minindn/c44/c44.ndnkey";
       std::string scheduleMember45 = "/tmp/minindn/time-location1.txt /tmp/minindn/c45/c45.ndnkey";
       std::string scheduleMember46 = "/tmp/minindn/time-location1.txt /tmp/minindn/c46/c46.ndnkey";
       std::string scheduleMember47 = "/tmp/minindn/time-location1.txt /tmp/minindn/c47/c47.ndnkey";
       std::string scheduleMember48 = "/tmp/minindn/time-location1.txt /tmp/minindn/c48/c48.ndnkey";
       std::string scheduleMember49 = "/tmp/minindn/time-location1.txt /tmp/minindn/c49/c49.ndnkey";
       std::string scheduleMember50 = "/tmp/minindn/time-location1.txt /tmp/minindn/c50/c50.ndnkey";

       std::string scheduleMember51 = "/tmp/minindn/time-location1.txt /tmp/minindn/c51/c51.ndnkey";
       std::string scheduleMember52 = "/tmp/minindn/time-location1.txt /tmp/minindn/c52/c52.ndnkey";
       std::string scheduleMember53 = "/tmp/minindn/time-location1.txt /tmp/minindn/c53/c53.ndnkey";
       std::string scheduleMember54 = "/tmp/minindn/time-location1.txt /tmp/minindn/c54/c54.ndnkey";
       std::string scheduleMember55 = "/tmp/minindn/time-location1.txt /tmp/minindn/c55/c55.ndnkey";
       std::string scheduleMember56 = "/tmp/minindn/time-location1.txt /tmp/minindn/c56/c56.ndnkey";
       std::string scheduleMember57 = "/tmp/minindn/time-location1.txt /tmp/minindn/c57/c57.ndnkey";
       std::string scheduleMember58 = "/tmp/minindn/time-location1.txt /tmp/minindn/c58/c58.ndnkey";
       std::string scheduleMember59 = "/tmp/minindn/time-location1.txt /tmp/minindn/c59/c59.ndnkey";
       std::string scheduleMember60 = "/tmp/minindn/time-location1.txt /tmp/minindn/c60/c60.ndnkey";

       std::string scheduleMember61 = "/tmp/minindn/time-location1.txt /tmp/minindn/c61/c61.ndnkey";
       std::string scheduleMember62 = "/tmp/minindn/time-location1.txt /tmp/minindn/c62/c62.ndnkey";
       std::string scheduleMember63 = "/tmp/minindn/time-location1.txt /tmp/minindn/c63/c63.ndnkey";
       std::string scheduleMember64 = "/tmp/minindn/time-location1.txt /tmp/minindn/c64/c64.ndnkey";
       std::string scheduleMember65 = "/tmp/minindn/time-location1.txt /tmp/minindn/c65/c65.ndnkey";
       std::string scheduleMember66 = "/tmp/minindn/time-location1.txt /tmp/minindn/c66/c66.ndnkey";
       std::string scheduleMember67 = "/tmp/minindn/time-location1.txt /tmp/minindn/c67/c67.ndnkey";
       std::string scheduleMember68 = "/tmp/minindn/time-location1.txt /tmp/minindn/c68/c68.ndnkey";
       std::string scheduleMember69 = "/tmp/minindn/time-location1.txt /tmp/minindn/c69/c69.ndnkey";
       std::string scheduleMember70 = "/tmp/minindn/time-location1.txt /tmp/minindn/c70/c70.ndnkey";

       std::string scheduleMember71 = "/tmp/minindn/time-location1.txt /tmp/minindn/c71/c71.ndnkey";
       std::string scheduleMember72 = "/tmp/minindn/time-location1.txt /tmp/minindn/c72/c72.ndnkey";
       std::string scheduleMember73 = "/tmp/minindn/time-location1.txt /tmp/minindn/c73/c73.ndnkey";
       std::string scheduleMember74 = "/tmp/minindn/time-location1.txt /tmp/minindn/c74/c74.ndnkey";
       std::string scheduleMember75 = "/tmp/minindn/time-location1.txt /tmp/minindn/c75/c75.ndnkey";
       std::string scheduleMember76 = "/tmp/minindn/time-location1.txt /tmp/minindn/c76/c76.ndnkey";
       std::string scheduleMember77 = "/tmp/minindn/time-location1.txt /tmp/minindn/c77/c77.ndnkey";
       std::string scheduleMember78 = "/tmp/minindn/time-location1.txt /tmp/minindn/c78/c78.ndnkey";
       std::string scheduleMember79 = "/tmp/minindn/time-location1.txt /tmp/minindn/c79/c79.ndnkey";
       std::string scheduleMember80 = "/tmp/minindn/time-location1.txt /tmp/minindn/c80/c80.ndnkey";

       std::string scheduleMember81 = "/tmp/minindn/time-location1.txt /tmp/minindn/c81/c81.ndnkey";
       std::string scheduleMember82 = "/tmp/minindn/time-location1.txt /tmp/minindn/c82/c82.ndnkey";
       std::string scheduleMember83 = "/tmp/minindn/time-location1.txt /tmp/minindn/c83/c83.ndnkey";
       std::string scheduleMember84 = "/tmp/minindn/time-location1.txt /tmp/minindn/c84/c84.ndnkey";
       std::string scheduleMember85 = "/tmp/minindn/time-location1.txt /tmp/minindn/c85/c85.ndnkey";
       std::string scheduleMember86 = "/tmp/minindn/time-location1.txt /tmp/minindn/c86/c86.ndnkey";
       std::string scheduleMember87 = "/tmp/minindn/time-location1.txt /tmp/minindn/c87/c87.ndnkey";
       std::string scheduleMember88 = "/tmp/minindn/time-location1.txt /tmp/minindn/c88/c88.ndnkey";
       std::string scheduleMember89 = "/tmp/minindn/time-location1.txt /tmp/minindn/c89/c89.ndnkey";
       std::string scheduleMember90 = "/tmp/minindn/time-location1.txt /tmp/minindn/c90/c90.ndnkey";

       std::string scheduleMember91 = "/tmp/minindn/time-location1.txt /tmp/minindn/c91/c91.ndnkey";
       std::string scheduleMember92 = "/tmp/minindn/time-location1.txt /tmp/minindn/c92/c92.ndnkey";
       std::string scheduleMember93 = "/tmp/minindn/time-location1.txt /tmp/minindn/c93/c93.ndnkey";
       std::string scheduleMember94 = "/tmp/minindn/time-location1.txt /tmp/minindn/c94/c94.ndnkey";
       std::string scheduleMember95 = "/tmp/minindn/time-location1.txt /tmp/minindn/c95/c95.ndnkey";
       std::string scheduleMember96 = "/tmp/minindn/time-location1.txt /tmp/minindn/c96/c96.ndnkey";
       std::string scheduleMember97 = "/tmp/minindn/time-location1.txt /tmp/minindn/c97/c97.ndnkey";
       std::string scheduleMember98 = "/tmp/minindn/time-location1.txt /tmp/minindn/c98/c98.ndnkey";
       std::string scheduleMember99 = "/tmp/minindn/time-location1.txt /tmp/minindn/c99/c99.ndnkey";
       std::string scheduleMember100 = "/tmp/minindn/time-location1.txt /tmp/minindn/c100/c100.ndnkey";

        manager.nac_add_member_location_time(scheduleMember1); 
        manager.nac_add_member_location_time(scheduleMember2);
        manager.nac_add_member_location_time(scheduleMember3); 
        manager.nac_add_member_location_time(scheduleMember4); 
        manager.nac_add_member_location_time(scheduleMember5); 
        manager.nac_add_member_location_time(scheduleMember6); 
        manager.nac_add_member_location_time(scheduleMember7); 
        manager.nac_add_member_location_time(scheduleMember8); 
        manager.nac_add_member_location_time(scheduleMember9); 
        manager.nac_add_member_location_time(scheduleMember10); 

        manager.nac_add_member_location_time(scheduleMember11); 
         manager.nac_add_member_location_time(scheduleMember12);
         manager.nac_add_member_location_time(scheduleMember13); 
         manager.nac_add_member_location_time(scheduleMember14); 
         manager.nac_add_member_location_time(scheduleMember15); 
         manager.nac_add_member_location_time(scheduleMember16); 
         manager.nac_add_member_location_time(scheduleMember17); 
         manager.nac_add_member_location_time(scheduleMember18); 
         manager.nac_add_member_location_time(scheduleMember19); 
         manager.nac_add_member_location_time(scheduleMember20); 

                 manager.nac_add_member_location_time(scheduleMember21); 
         manager.nac_add_member_location_time(scheduleMember22);
         manager.nac_add_member_location_time(scheduleMember23); 
         manager.nac_add_member_location_time(scheduleMember24); 
         manager.nac_add_member_location_time(scheduleMember25); 
         manager.nac_add_member_location_time(scheduleMember26); 
         manager.nac_add_member_location_time(scheduleMember27); 
         manager.nac_add_member_location_time(scheduleMember28); 
         manager.nac_add_member_location_time(scheduleMember29); 
         manager.nac_add_member_location_time(scheduleMember30); 

                 manager.nac_add_member_location_time(scheduleMember31); 
         manager.nac_add_member_location_time(scheduleMember32);
         manager.nac_add_member_location_time(scheduleMember33); 
         manager.nac_add_member_location_time(scheduleMember34); 
         manager.nac_add_member_location_time(scheduleMember35); 
         manager.nac_add_member_location_time(scheduleMember36); 
         manager.nac_add_member_location_time(scheduleMember37); 
         manager.nac_add_member_location_time(scheduleMember38); 
         manager.nac_add_member_location_time(scheduleMember39); 
         manager.nac_add_member_location_time(scheduleMember40); 

                 manager.nac_add_member_location_time(scheduleMember41); 
         manager.nac_add_member_location_time(scheduleMember42);
         manager.nac_add_member_location_time(scheduleMember43); 
         manager.nac_add_member_location_time(scheduleMember44); 
         manager.nac_add_member_location_time(scheduleMember45); 
         manager.nac_add_member_location_time(scheduleMember46); 
         manager.nac_add_member_location_time(scheduleMember47); 
         manager.nac_add_member_location_time(scheduleMember48); 
         manager.nac_add_member_location_time(scheduleMember49); 
         manager.nac_add_member_location_time(scheduleMember50); 

                 manager.nac_add_member_location_time(scheduleMember51); 
         manager.nac_add_member_location_time(scheduleMember52);
         manager.nac_add_member_location_time(scheduleMember53); 
         manager.nac_add_member_location_time(scheduleMember54); 
         manager.nac_add_member_location_time(scheduleMember55); 
         manager.nac_add_member_location_time(scheduleMember56); 
         manager.nac_add_member_location_time(scheduleMember57); 
         manager.nac_add_member_location_time(scheduleMember58); 
         manager.nac_add_member_location_time(scheduleMember59); 
         manager.nac_add_member_location_time(scheduleMember60); 

                 manager.nac_add_member_location_time(scheduleMember61); 
         manager.nac_add_member_location_time(scheduleMember62);
         manager.nac_add_member_location_time(scheduleMember63); 
         manager.nac_add_member_location_time(scheduleMember64); 
         manager.nac_add_member_location_time(scheduleMember65); 
         manager.nac_add_member_location_time(scheduleMember66); 
         manager.nac_add_member_location_time(scheduleMember67); 
         manager.nac_add_member_location_time(scheduleMember68); 
         manager.nac_add_member_location_time(scheduleMember69); 
         manager.nac_add_member_location_time(scheduleMember70); 

         manager.nac_add_member_location_time(scheduleMember71); 
         manager.nac_add_member_location_time(scheduleMember72);
         manager.nac_add_member_location_time(scheduleMember73); 
         manager.nac_add_member_location_time(scheduleMember74); 
         manager.nac_add_member_location_time(scheduleMember75); 
         manager.nac_add_member_location_time(scheduleMember76); 
         manager.nac_add_member_location_time(scheduleMember77); 
         manager.nac_add_member_location_time(scheduleMember78); 
         manager.nac_add_member_location_time(scheduleMember79); 
         manager.nac_add_member_location_time(scheduleMember80); 

         manager.nac_add_member_location_time(scheduleMember81); 
         manager.nac_add_member_location_time(scheduleMember82);
         manager.nac_add_member_location_time(scheduleMember83); 
         manager.nac_add_member_location_time(scheduleMember84); 
         manager.nac_add_member_location_time(scheduleMember85); 
         manager.nac_add_member_location_time(scheduleMember86); 
         manager.nac_add_member_location_time(scheduleMember87); 
         manager.nac_add_member_location_time(scheduleMember88); 
         manager.nac_add_member_location_time(scheduleMember89); 
         manager.nac_add_member_location_time(scheduleMember90); 

         manager.nac_add_member_location_time(scheduleMember91); 
         manager.nac_add_member_location_time(scheduleMember92);
         manager.nac_add_member_location_time(scheduleMember93); 
         manager.nac_add_member_location_time(scheduleMember94); 
         manager.nac_add_member_location_time(scheduleMember95); 
         manager.nac_add_member_location_time(scheduleMember96); 
         manager.nac_add_member_location_time(scheduleMember97); 
         manager.nac_add_member_location_time(scheduleMember98); 
         manager.nac_add_member_location_time(scheduleMember99); 
         manager.nac_add_member_location_time(scheduleMember100); 

        }

        

        // std::cout << "Enter time-shcedule and member: "<< std::endl;
        // while(getline(std::cin, scheduleMember)&&scheduleMember.length() > 0){
        //     manager.nac_add_member(scheduleMember); 
        // }

        // std::cout << "Enter location-schedule and member: "<< std::endl;
        // while(getline(std::cin, scheduleMember)&&scheduleMember.length() > 0){
        //     manager.nac_add_member_location(scheduleMember); 
        // }

        // std::cout << "Enter time-location-schedule and member: "<< std::endl;
        // while(getline(std::cin, scheduleMember)&&scheduleMember.length() > 0){
        //     manager.nac_add_member_location_time(scheduleMember); 
        // }

        std::cout <<"Set manager successfully!" << std::endl;
        // return producer.nac_add_member(argc - 1, argv + 1); 
        try {
            manager.run();
        }
        catch (const std::exception& e) {
            std::cerr << "ERROR: " << e.what() << std::endl;
        }
        return 0;

    }
    else {
      std::cerr << nac_helper << std::endl;
      return 1;
    }
  }
  catch (const std::exception& e) {
    std::cerr << "ERROR: " << e.what();
    return 1;
  }
}
