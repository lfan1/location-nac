/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/**
 * Copyright (c) 2014-2018, Regents of the University of California
 *
 * NAC library is free software: you can redistribute it and/or modify it under the
 * terms of the GNU Lesser General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * NAC library is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more details.
 *
 * You should have received copies of the GNU General Public License and GNU Lesser
 * General Public License along with ndn-cxx, e.g., in COPYING.md file.  If not, see
 * <http://www.gnu.org/licenses/>.
 *
 * See AUTHORS.md for complete list of NAC library authors and contributors.
 */

#include <ndn-cxx/face.hpp>
#include <ndn-cxx/security/validator-config.hpp>
#include <ndn-cxx/util/logger.hpp>

// #include <ndn-nac/decryptor.hpp>
#include "decryptor.hpp"

#include <iostream>
#include <boost/date_time/posix_time/posix_time.hpp>

#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <string>
#include <sstream> 
#include <boost/filesystem.hpp>
#include "ndn-cxx/util/io.hpp"
#include <boost/filesystem.hpp>

NDN_LOG_INIT(examples.ConsumerNACApp);

// Enclosing code in ndn simplifies coding (can also use `using namespace ndn`)
namespace ndn {
namespace nac {
// Additional nested namespaces can be used to prevent/limit name conflicts
namespace examples {

class Consumer : noncopyable
{
public:
  Consumer()
    : m_face(nullptr, m_keyChain)
    , m_validator(m_face)
    // , m_decryptor(m_keyChain.createIdentity(Name("/localhost/operator-new"), RsaKeyParams()).getDefaultKey(), m_validator, m_keyChain, m_face)
    , m_decryptor(m_keyChain.getPib().getDefaultIdentity().getDefaultKey(), m_validator, m_keyChain, m_face)
  {
    m_validator.load(R"CONF(
        trust-anchor
        {
          type any
        }
      )CONF", "fake-config");
  }

  static const time::system_clock::TimePoint
getCkEndTimeslot(const time::system_clock::TimePoint& timeslot, const time::milliseconds granuality)
{
  return time::fromUnixTimestamp(time::toUnixTimestamp(timeslot) + granuality);
}

  void
  run() 
  {
    Name iName = Name("/org/md2k/DATA/data");

    std::string starttime = "20200525T080059";
    std::string endtime = "20200525T124639";

    // // std::string endtime = "20200525T115959";


    time::system_clock::TimePoint s_ts;
    s_ts = time::fromIsoString(starttime);

    time::system_clock::TimePoint e_ts;
    e_ts = time::fromIsoString(endtime);

    time::system_clock::TimePoint next_ts;
    next_ts = s_ts;

    bool change = false;

    // time::milliseconds granularity = time::milliseconds(1*60000);;

    NDN_LOG_INFO("data start decryption " );

    while(next_ts <= e_ts){

      if (next_ts > time::fromIsoString("20200525T081639")) {

        if (change == false) {

          next_ts = time::fromIsoString("20200525T123000");
          change = true; 

        }

        iName.append(time::toIsoString(next_ts)).append("35.120862").append("-89.936092").append("100");
        Interest interest(iName);
        interest.setInterestLifetime(2_s); // 2 seconds
        interest.setMustBeFresh(true);


        NDN_LOG_INFO("Sending Interest: " << interest);
	NDN_LOG_INFO( "consumer express interest for data " << interest );
        m_face.expressInterest(interest,
                              bind(&Consumer::onData, this,  _1, _2),
                              bind(&Consumer::onNack, this, _1, _2),
                              bind(&Consumer::onTimeout, this, _1));

      }
      else {
        iName.append(time::toIsoString(next_ts)).append("35.121185").append("-89.938107").append("60");
        Interest interest(iName);
        interest.setInterestLifetime(2_s); // 2 seconds
        interest.setMustBeFresh(true);


        NDN_LOG_INFO("Sending Interest: " << interest);
	NDN_LOG_INFO( "consumer express interest for data " << interest );
        m_face.expressInterest(interest,
                              bind(&Consumer::onData, this,  _1, _2),
                              bind(&Consumer::onNack, this, _1, _2),
                              bind(&Consumer::onTimeout, this, _1));

      }


      // NDN_LOG_INFO("Sending Interest: " << interest);

      // m_face.expressInterest(interest,
      //                       bind(&Consumer::onData, this,  _1, _2),
      //                       bind(&Consumer::onNack, this, _1, _2),
      //                       bind(&Consumer::onTimeout, this, _1));

      // processEvents will block until the requested data received or timeout occurs
      m_face.processEvents();
      next_ts = time::fromUnixTimestamp(time::toUnixTimestamp(next_ts) + time::milliseconds(1*1000));
      iName = Name("/org/md2k/DATA/data");
    }

    NDN_LOG_INFO("data finish decryption " );
  }

private:
  void
  onData(const Interest& interest, const Data& data)
  {

    NDN_LOG_INFO( "consumer data call back ");
    if (data.getMetaInfo().getType() == ndn::tlv::ContentType_Nack) {

      const uint8_t* bytes = data.getContent().value();
      const int len = data.getContent().value_size();

      std::string s(reinterpret_cast<char const*>(bytes), len);

      NDN_LOG_INFO("received Nack with reason: " << s
              << " for interest " << interest);

      return;
    }
    NDN_LOG_INFO( "consumer start decryption ");
    m_validator.validate(data,
      [=] (const Data& data) {
        std::cout << "Get encrypted data: \n"<< data << std::endl;
//	NDN_LOG_INFO( "consumer finish decryption" );
        NDN_LOG_INFO("Get encrypted data: \n"<< data );
        m_decryptor.decrypt(data.getContent().blockFromValue(),
          [=] (ConstBufferPtr content) {

	    NDN_LOG_INFO( "consumer finish decryption" );

            NDN_LOG_INFO(" Decrypted content: \n"<< std::string(reinterpret_cast<const char*>(content->data()), content->size()) );
    
          },
          [=] (const ErrorCode&, const std::string& error) {
            std::cerr << "Cannot decrypt data: " << error << std::endl;
          });
      },
      [=] (const Data& data, const ValidationError& error) {
        std::cerr << "Cannot validate retrieved data: " << error << std::endl;
      });
  }

  void
  onNack(const Interest& interest, const lp::Nack& nack)
  {
    std::cout << "received Nack with reason " << nack.getReason()
              << " for interest " << interest << std::endl;
  }

  void
  onTimeout(const Interest& interest)
  {
    std::cout << "Timeout " << interest << std::endl;
  }

private:
  KeyChain m_keyChain;
  Face m_face;
  ValidatorConfig m_validator;
  Decryptor m_decryptor;
};

} // namespace examples
} // namespace nac
} // namespace ndn

int
main(int argc, char** argv)
{
  ndn::nac::examples::Consumer consumer;
  try {
    consumer.run();
  }
  catch (const std::exception& e) {
    std::cerr << "ERROR: " << e.what() << std::endl;
  }
  return 0;
}
