#!/bin/bash

scp -P 61267 manager mini-ndn@europa.cs.memphis.edu:/tmp/minindn/p
scp -P 61267 producer-sync mini-ndn@europa.cs.memphis.edu:/tmp/minindn/p

for (( i=1; i<=100; i++ ))
do  
   echo "copy to /tmp/minindn/c$i"
   scp -P 61267 consumer1 mini-ndn@europa.cs.memphis.edu:/tmp/minindn/c$i
done
